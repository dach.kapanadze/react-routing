import { createAsyncThunk } from "@reduxjs/toolkit";
export const subscribeUser = createAsyncThunk(
  "subscription/subscribeUser",
  async ({ email, isSubscribed }, { rejectWithValue }) => {
    try {
      const endpoint = isSubscribed ? "unsubscribe" : "subscribe";
      const response = await fetch(`http://localhost:3000/${endpoint}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });

      if (!response.ok) {
        const errorData = await response.json();
        return rejectWithValue(errorData);
      }

      return response.json();
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const unsubscribeUser = createAsyncThunk(
  "subscription/unsubscribeUser",
  async (_, { rejectWithValue }) => {
    try {
      const response = await fetch("http://localhost:3000/unsubscribe", {
        method: "POST",
      });

      if (!response.ok) {
        const errorData = await response.json();
        return rejectWithValue(errorData);
      }

      return response.json();
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);
