import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const subscribeUser = createAsyncThunk(
  "subscription/subscribeUser",
  async (email, { rejectWithValue }) => {
    try {
      const response = await axios.post("http://localhost:3000/subscribe", {
        email: email,
      });

      if (response.status === 200) {
        return response.data;
      } else {
        return rejectWithValue(response.statusText);
      }
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

const subscriptionSlice = createSlice({
  name: "subscription",
  initialState: {
    isSubscribed: false,
    isLoading: false,
    error: null,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(subscribeUser.pending, (state) => {
        state.isLoading = true;
        state.error = null;
      })
      .addCase(subscribeUser.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSubscribed = !state.isSubscribed;
      })
      .addCase(subscribeUser.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      });
  },
});

export const selectIsSubscribed = (state) => state.subscription.isSubscribed;
export const selectLoadingStatus = (state) => state.subscription.isLoading;
export const selectError = (state) => state.subscription.error;

export default subscriptionSlice.reducer;
