import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { toggleVisibility } from "./Redux/visibilityReducer.js";

const CommunitySection = () => {
  const isVisible = useSelector((state) => state.visibility.isVisible);
  const dispatch = useDispatch();

  const sectionVis = () => {
    dispatch(toggleVisibility());
  };
  const [communityData, setCommunityData] = useState([]);

  useEffect(() => {
    const fetchCommunityData = async () => {
      try {
        const response = await fetch("http://localhost:3000/community");
        if (response.ok) {
          const data = await response.json();
          setCommunityData(data);
        } else {
          console.error("Error fetching community data:", response.status);
        }
      } catch (error) {
        console.error("Error fetching community data:", error);
      }
    };

    fetchCommunityData();
  }, []);

  return (
    <section className="app-section app-section--community">
      <h2 className="app-title">
        Big Community of <br /> People Like You
      </h2>
      <button className="hide-section" onClick={sectionVis}>
        {isVisible ? "Hide section" : "Show section"}{" "}
      </button>
      {isVisible && (
        <>
          <h3 className="app-subtitle">
            We’re proud of our products, and we’re really excited
            <br /> when we get feedback from our users.
          </h3>
          <div className="testimonials-container">
            {communityData.map((person) => (
              <div key={person.id} className="testimonial">
                <img
                  className="testimonial-img"
                  src={person.avatar}
                  alt={`${person.firstName} ${person.lastName}`}
                />
                <p className="testimonial-text">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolor.
                </p>
                <div className="testimonial-author">
                  <Link to={`/community/${person.id}`} className="author-info">
                    <div className="testimonial-author-firstname">
                      {person.firstName}
                    </div>
                    <div className="testimonial-author-lastname">
                      {person.lastName}
                    </div>
                  </Link>
                </div>
                <div className="testimonial-position">{person.position}</div>
              </div>
            ))}
          </div>
        </>
      )}
    </section>
  );
};

export default CommunitySection;
