import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import UserDetailsPage from "./userDetails";
import "./index.css";
import JoinUsSection from "./joinOurProgramSection";
import CommunitySection from "./CommunitySection";
import NotFound from "./NotFound";
import { setData } from "./Redux/usersReducer";
import { useDispatch } from "react-redux";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("http://localhost:3000/community");
        if (!response.ok) throw new Error("There is problem");
        const jsonData = await response.json();
        // setData(jsonData)
        dispatch(setData(jsonData));
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, [dispatch]);

  return (
    <Routes>
      <Route path="/" element={<JoinUsSection />}></Route>
      <Route path="/community" element={<CommunitySection />}></Route>
      <Route path="/community/:id" element={<UserDetailsPage />} />
      <Route path="/*" element={<NotFound />}></Route>
    </Routes>
  );
}

export default App;
